const express = require('express');
const bodyParser = require('body-parser');
const sessionHandler = require('express-session');
const cookieParser = require('cookie-parser');

const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('./models');

const app = express();
const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });
const port = '3000';

// const oneDay = 1000 * 60 * 60 * 24;

// app.use(
//   sessionHandler({
//     secret: 'secret',
//     saveUninitialized: true,
//     cookie: { maxAge: oneDay },
//     resave: false,
//   })
// );

// app.use(
//   express.urlencoded({
//     extended: false,
//   })
// );

app.use(cookieParser());

app.set('view engine', 'ejs');

// REGION USER GAMES
app.get('/', (req, res) => {
  user_games
    .findAll({
      where: {},
      order: [
        ['updatedAt', 'desc'],
        ['createdAt', 'desc'],
      ],
      // include: Phone
    })
    .then((userGames) => {
      res.render('user_games/index', {
        // session: req.session,
        userGames: userGames,
      });
    });
});

app.get('/createUserGames', (req, res) => {
  res.render('user_games/create', {
    // session: req.session,
  });
});

app.get('/edit/:id', (req, res) => {
  user_games.findByPk(req.params.id).then((userGames) => {
    res.render('user_games/edit', {
      // session: req.session,
      userGames: userGames,
    });
  });
});

app.get('/show/:id', (req, res) => {
  user_games.findByPk(req.params.id).then((userGames) => {
    res.render('user_games/show', {
      // session: req.session,
      userGames: userGames,
    });
  });
});

app.get('/show/:id/biodata', (req, res) => {
  user_games.findByPk(req.params.id).then((userGames) => {
    res.render('user_games_biodata/create', {
      // session: req.session,
      userGames: userGames,
    });
  });
});

// REGION BIODATA
app.get('/userGamesBiodata', (req, res) => {
  user_games_biodata
    .findAll({
      where: {},
      order: [
        ['updatedAt', 'desc'],
        ['createdAt', 'desc'],
      ],
    })
    .then((userGamesBiodata) => {
      res.render('user_games_biodata/index', {
        // session: req.session,
        userGamesBiodata: userGamesBiodata,
      });
    });
});

app.get('/editBiodata/:id', (req, res) => {
  user_games_biodata.findByPk(req.params.id).then((userGamesBiodata) => {
    res.render('user_games_biodata/edit', {
      // session: req.session,
      userGamesBiodata: userGamesBiodata,
    });
  });
});

// app.get('/show/:id/history', (req, res) => {
//   user_games.findByPk(req.params.id).then((userGames) => {
//     res.render('user_games_history/create', {
//       // session: req.session,
//       userGames: userGames,
//     });
//   });
// });

app.get('/show/:id/history', (req, res) => {
  user_games.findByPk(req.params.id).then((userGames) => {
    res.render('user_games_history/create', {
      // session: req.session,
      userGames: userGames,
    });
  });
});

app.get('/userGamesHistory', (req, res) => {
  user_games_history
    .findAll({
      where: {},
      order: [
        ['updatedAt', 'desc'],
        ['createdAt', 'desc'],
      ],
    })
    .then((userGamesHistory) => {
      res.render('user_games_history/index', {
        // session: req.session,
        userGamesHistory: userGamesHistory,
      });
    });
});

app.get('/login', (req, res) => {
  if (req.session.userId) {
    return res.redirect('/');
  }

  res.render('users/login', {
    // session: req.session,
  });
});

// API
app.post('/api/usergames', urlEncoded, async (req, res) => {
  try {
    let userGames = await user_games.create({
      username: req.body.username,
      password: req.body.password,
    });

    res.redirect('/');
  } catch (error) {
    res.render('user_games/create', {
      error: error,
    });
  }
});

app.post('/api/usergames/edit', urlEncoded, async (req, res) => {
  try {
    let userGames = await user_games.findByPk(req.body.id);
    userGames.username = req.body.username
      ? req.body.username
      : userGames.username;
    userGames.password = req.body.password
      ? req.body.password
      : userGames.password;
    await userGames.save();

    res.redirect('/');
  } catch (error) {
    res.render('user_games/edit', {
      error: error,
    });
  }
});

app.get('/api/usergames/delete/:id', async (req, res) => {
  try {
    const count = await user_games.destroy({
      where: { id: req.params.id },
    });

    if (count > 0) {
      return res.redirect('/');
    } else {
      return res.redirect('/');
    }
  } catch (err) {
    return res.redirect('/');
  }
});

app.post('/api/usergames/biodata', urlEncoded, async (req, res) => {
  try {
    let phone = await user_games_biodata.create({
      // userId: parseInt(req.body.userId),
      userId: req.body.userId,
      fullname: req.body.fullname,
      dob: req.body.dob,
      location: req.body.location,
    });

    res.redirect(`/show/${req.body.userId}`);
  } catch (error) {
    res.render('user_games_biodata/create', {
      error: error,
      user_games: user_games.findByPk(req.body.userId),
    });
  }
});

app.post('/api/usergamesbiodata/edit', urlEncoded, async (req, res) => {
  try {
    let userGamesBiodata = await user_games_biodata.findByPk(req.body.id);
    userGamesBiodata.fullname = req.body.fullname
      ? req.body.fullname
      : userGamesBiodata.fullname;
    userGamesBiodata.dob = req.body.dob ? req.body.dob : userGamesBiodata.dob;
    userGamesBiodata.location = req.body.location
      ? req.body.location
      : userGamesBiodata.location;
    await userGamesBiodata.save();

    res.redirect('/userGamesBiodata');
  } catch (error) {
    res.render('user_games_biodata/edit', {
      error: error,
    });
  }
});

app.post('/api/usergames/history', urlEncoded, async (req, res) => {
  try {
    let phone = await user_games_history.create({
      // userId: parseInt(req.body.userId),
      userId: req.body.userId,
      game_name: req.body.game_name,
      time_played: req.body.time_played,
      score: req.body.score,
    });

    console.log(phone);

    res.redirect(`/show/${req.body.userId}`);
  } catch (error) {
    res.render('user_games_history/create', {
      error: error,
      // session: req.session,
      user_games: user_games.findByPk(req.body.userId),
    });
  }
});

app.post('/api/login', urlEncoded, async (req, res) => {
  let message = {
    type: 'error',
    message: 'Login failed, username/password does not match',
  };

  try {
    let user = await user_games.findOne({
      where: {
        username: req.body.username,
      },
    });

    if (!user.validPassword(req.body.password)) {
      return res.render('users/login', {
        message: message,
        // session: req.session,
      });
    }

    // Store to session
    req.session.userId = user.id;
    res.redirect('/login');
  } catch (error) {
    res.render('users/login', {
      message: message,
      // session: req.session,
    });
  }
});

app.get('/api/logout', urlEncoded, async (req, res) => {
  if (req.session.userId) {
    req.session.destroy();
    res.redirect('/');
  }
});

app.listen(port, () => {
  console.log(`App listening on port: ${port}`);
});
