'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_games_biodata.belongsTo(models.user_games, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });
    }
  }
  user_games_biodata.init(
    {
      fullname: DataTypes.STRING,
      dob: DataTypes.STRING,
      location: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'user_games_biodata',
    }
  );
  return user_games_biodata;
};
